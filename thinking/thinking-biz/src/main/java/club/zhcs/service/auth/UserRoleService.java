package club.zhcs.service.auth;

import java.util.List;

import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;

import club.zhcs.bean.auth.UserRole;
import club.zhcs.dao.acl.UserRoleDao;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 用户角色关系业务
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午3:00:50
 */
@IocBean
public class UserRoleService {
	@Inject
	UserRoleDao userRoleDao;

	/**
	 * 根据用户 id 查询
	 * 
	 * @param id
	 *            用户 id
	 * @return 用户角色关系列表
	 *
	 * @author 王贵源
	 */
	public List<UserRole> findByUserId(int id) {
		return userRoleDao.search(Cnd.where("userId", "=", id));
	}

	/**
	 * 添加用户角色关系
	 * 
	 * @param ur
	 * @return
	 *
	 * @author 王贵源
	 */
	public UserRole addUserRole(UserRole ur) {
		return userRoleDao.save(ur);
	}

	/**
	 * 根据角色 id 查询
	 * 
	 * @param id
	 *            角色 id
	 * @return 用户角色关系列表
	 *
	 * @author 王贵源
	 */
	public List<UserRole> findByRoleId(int id) {
		return userRoleDao.search(Cnd.where("roleId", "=", id));
	}

}

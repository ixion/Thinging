package club.zhcs.service.auth;

import java.util.List;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.entity.Record;
import org.nutz.dao.sql.Sql;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;

import com.kerbores.utils.db.SqlActuator;

import club.zhcs.bean.auth.RolePermission;
import club.zhcs.dao.acl.RolePermissionDao;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 角色权限关系业务
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午2:44:28
 */
@IocBean
public class RolePermissionService {
	@Inject
	RolePermissionDao rolePermissionDao;

	/**
	 * 根据权限 id 查询
	 * 
	 * @param id
	 *            权限 id
	 * @return 关系列表
	 *
	 * @author 王贵源
	 */
	public List<RolePermission> findByPermissionId(int id) {
		return rolePermissionDao.search(Cnd.where("permissionId", "=", id));
	}

	/**
	 * 根据角色 id 查询
	 * 
	 * @param id
	 *            角色 id
	 * @return 关系列表
	 *
	 * @author 王贵源
	 */
	public List<Record> findByRoleId(int id) {
		final Dao dao = rolePermissionDao.getDao();
		Sql sql = dao.sqls().create("find.role.permission.by.role.id");
		sql.params().set("id", id);
		return SqlActuator.runReport(sql, dao);
	}

}

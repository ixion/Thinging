package club.zhcs.service.auth;

import java.util.List;

import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;

import club.zhcs.bean.auth.UserPermission;
import club.zhcs.dao.acl.UserPermissionDao;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 用户权限关系业务
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午2:59:16
 */
@IocBean
public class UserPermissionService {

	@Inject
	UserPermissionDao userPermissionDao;

	public UserPermission save(UserPermission up) {
		return userPermissionDao.save(up);
	}

	/**
	 * 根据权限 id 查询
	 * 
	 * @param id
	 *            权限 id
	 * @return 用户权限关系列表
	 *
	 * @author 王贵源
	 */
	public List<UserPermission> findByPermissionId(int id) {
		return userPermissionDao.search(Cnd.where("permissionId", "=", id));
	}
}

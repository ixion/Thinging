package club.zhcs.ext.permission;

import java.util.concurrent.TimeUnit;

import org.nutz.log.Log;
import org.nutz.log.Logs;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 权限检查器(有缓存)
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午2:32:16
 */
public class PermissionChecker {

	LoadingCache<String, Boolean> cache;
	Log log = Logs.get();

	/**
	 * 缓存单例
	 * 
	 * @return
	 *
	 * @author 王贵源
	 */
	public LoadingCache<String, Boolean> getCache() {
		if (cache == null) {
			cache = get();
		}
		return cache;
	}

	/**
	 * 检查逻辑
	 * 
	 * @return 状态
	 *
	 * @author 王贵源
	 */
	private LoadingCache<String, Boolean> get() {
		return CacheBuilder.newBuilder().maximumSize(2000).expireAfterAccess(5, TimeUnit.MINUTES).removalListener(new RemovalListener<String, Boolean>() {

			@Override
			public void onRemoval(RemovalNotification<String, Boolean> notification) {
				log.debug(notification.getKey() + " removed....");
			}
		}).build(new CacheLoader<String, Boolean>() {

			@Override
			public Boolean load(String key) throws Exception {
				log.debug(key + " loading.... ");
				return false;
			}
		});
	}

}

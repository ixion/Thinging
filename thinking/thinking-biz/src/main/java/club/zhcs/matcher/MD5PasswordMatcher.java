package club.zhcs.matcher;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.nutz.lang.Lang;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 基于 MD5 的 shiro 密码匹配器
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午2:35:54
 */
public class MD5PasswordMatcher extends SimpleCredentialsMatcher {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.shiro.authc.credential.SimpleCredentialsMatcher#
	 * doCredentialsMatch (org.apache.shiro.authc.AuthenticationToken,
	 * org.apache.shiro.authc.AuthenticationInfo)
	 */
	@Override
	public boolean doCredentialsMatch(AuthenticationToken authcToken, AuthenticationInfo info) {
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
		Object tokenCredentials = Lang.md5(new String(token.getPassword()));// MD5
		Object accountCredentials = getCredentials(info);
		return equals(tokenCredentials, accountCredentials);
	}
}

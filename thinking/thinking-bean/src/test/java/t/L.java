package t;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.nutz.dao.impl.NutDao;
import org.nutz.dao.impl.SimpleDataSource;
import org.nutz.json.Json;
import org.nutz.lang.ContinueLoop;
import org.nutz.lang.Each;
import org.nutz.lang.ExitLoop;
import org.nutz.lang.Files;
import org.nutz.lang.Lang;
import org.nutz.lang.LoopException;
import org.nutz.lang.util.NutMap;

import club.zhcs.bean.auth.Permission;

/**
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月28日 下午5:31:29
 */
public class L {

	SimpleDataSource ds;

	{
		ds = new SimpleDataSource();
		ds.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/menu?useUnicode=true&characterEncoding=utf8");
		ds.setUsername("root");
		ds.setPassword("123456");
	}

	@Before
	public void init() {
		dao.create(Permission.class, true);
	}

	NutDao dao = new NutDao(ds);

	@Test
	public void menu() {
		String info = Files.read("menu.js");
		List<Map> list = (List) Json.fromJson(info);
		Lang.each(list, new Each<Map>() {

			@Override
			public void invoke(int index, Map map, int length) throws ExitLoop, ContinueLoop, LoopException {
				NutMap data = NutMap.WRAP(map);
				save(data, null);
			}
		});
	}

	/**
	 * 
	 * @param data
	 * @param pName
	 *
	 * @author 王贵源
	 */
	protected void save(NutMap data, String pName) {
		if (hasSub(data)) {
			final Permission p = Lang.map2Object(data, Permission.class);
			p.setNeedPermission(pName);
			p.setInstalled(true);
			dao.insert(p);
			// 获取 sub 迭代 递归
			Lang.each(data.getList("sub", NutMap.class), new Each<NutMap>() {

				@Override
				public void invoke(int index, NutMap ele, int length) throws ExitLoop, ContinueLoop, LoopException {
					save(ele, p.getName());
				}
			});

		} else {
			Permission p = Lang.map2Object(data, Permission.class);
			p.setNeedPermission(pName);
			p.setInstalled(true);
			dao.insert(p);
		}
	}

	/**
	 * 
	 * @param data
	 * @return
	 *
	 * @author 王贵源
	 */
	protected boolean hasSub(NutMap data) {
		return data.get("sub") != null && !data.getList("sub", Object.class).isEmpty();
	}
}

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 值对象包
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午2:16:16
 */
package club.zhcs.vo;
package club.zhcs.vo;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 应用级常量
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午2:21:43
 */
public class Application {
	/**
	 * 页面大小
	 */
	public static final int PAGESIZE = 15;

	/**
	 * 应用名称
	 */
	public static final String NAME = "Thinking";

	/**
	 * 微信token
	 */
	public static final String WECHATTOKEN = "!QAZ2wsx";

	/**
	 * 百度地图appkey
	 */
	public static final String BAIDUMAPKEY = "dHF4Mm7zlaP0qGYNMu4AcaAG";

	/**
	 * 
	 * @author 王贵源
	 *
	 * @email kerbores@kerbores.com
	 *
	 * @description session 内容 key 常量
	 * 
	 * @copyright 内部代码,禁止转发
	 *
	 *
	 * @time 2016年1月26日 下午2:22:19
	 */
	public static class SessionKeys {
		/**
		 * 用戶在session中保存的key
		 */
		public static final String USER_KEY = "AD_USER";
	}

}

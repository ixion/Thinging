package club.zhcs.dao.acl;

import org.nutz.ioc.loader.annotation.IocBean;

import com.kerbores.utils.db.dao.impl.BaseDao;

import club.zhcs.bean.auth.User;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 用户数据访问
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午2:26:42
 */
@IocBean
public class UserDao extends BaseDao<User> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.dgj.utils.db.dao.impl.BaseDao#initClass()
	 */
	@Override
	public Class<User> initClass() {
		return User.class;
	}

}

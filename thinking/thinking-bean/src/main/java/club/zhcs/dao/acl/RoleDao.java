package club.zhcs.dao.acl;

import org.nutz.ioc.loader.annotation.IocBean;

import com.kerbores.utils.db.dao.impl.BaseDao;

import club.zhcs.bean.auth.Role;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 角色数据访问
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午2:26:12
 */
@IocBean
public class RoleDao extends BaseDao<Role> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.dgj.utils.db.dao.impl.BaseDao#initClass()
	 */
	@Override
	public Class<Role> initClass() {
		return Role.class;
	}

}

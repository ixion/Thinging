package club.zhcs.dao.acl;

import org.nutz.ioc.loader.annotation.IocBean;

import com.kerbores.utils.db.dao.impl.BaseDao;

import club.zhcs.bean.auth.UserPermission;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 用户权限关系数据访问
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午2:26:56
 */
@IocBean
public class UserPermissionDao extends BaseDao<UserPermission> {

	/*
	 * (非 Javadoc) <p>Title: initClass</p> <p>Description: </p>
	 * 
	 * @return
	 * 
	 * @see com.dgj.utils.db.dao.impl.BaseDao#initClass()
	 */
	@Override
	public Class<UserPermission> initClass() {
		return UserPermission.class;
	}

}

/**
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description acl 相关数据访问
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午2:25:27
 */
package club.zhcs.dao.acl;
package club.zhcs.dao.acl;

import org.nutz.ioc.loader.annotation.IocBean;

import com.kerbores.utils.db.dao.impl.BaseDao;

import club.zhcs.bean.auth.RolePermission;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 角色权限关系数据访问
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午2:26:24
 */
@IocBean
public class RolePermissionDao extends BaseDao<RolePermission> {

	/*
	 * (非 Javadoc) <p>Title: initClass</p> <p>Description: </p>
	 * 
	 * @return
	 * 
	 * @see com.dgj.utils.db.dao.impl.BaseDao#initClass()
	 */
	@Override
	public Class<RolePermission> initClass() {
		return RolePermission.class;
	}

}

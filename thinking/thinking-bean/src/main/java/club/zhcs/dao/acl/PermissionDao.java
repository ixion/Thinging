package club.zhcs.dao.acl;

import org.nutz.ioc.loader.annotation.IocBean;

import com.kerbores.utils.db.dao.impl.BaseDao;

import club.zhcs.bean.auth.Permission;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 权限数据访问
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午2:25:55
 */
@IocBean
public class PermissionDao extends BaseDao<Permission> {
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.dgj.utils.db.dao.impl.BaseDao#initClass()
	 */
	@Override
	public Class<Permission> initClass() {
		return Permission.class;
	}
}

// package club.zhcs.tools;
//
// import org.apache.velocity.tools.config.DefaultKey;
// import org.apache.velocity.tools.config.InvalidScope;
// import org.nutz.mvc.Mvcs;
//
// import club.zhcs.bean.auth.User;
// import club.zhcs.vo.Application.SessionKeys;
//
/// **
// * @author 王贵源<kerbores>
// *
// * create at 2015年12月11日 下午2:03:50
// */
// @DefaultKey("S")
// @InvalidScope({ "application" })
// public class SessionUtils {
//
// /**
// * 当前登录用户
// *
// * @return
// */
// public static User loginUser() {
// return (User) Mvcs.getReq().getSession().getAttribute(SessionKeys.USER_KEY);
// }
//
// }

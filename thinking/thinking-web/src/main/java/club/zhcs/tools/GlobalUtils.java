// package club.zhcs.tools;
//
// import org.apache.velocity.tools.config.DefaultKey;
// import org.apache.velocity.tools.config.InvalidScope;
// import org.nutz.lang.Strings;
// import org.nutz.mvc.Mvcs;
//
// import com.kerbores.utils.common.Numbers;
//
/// **
// * @author xiaoshan 2015年6月10日 下午4:01:43
// */
// @DefaultKey("G")
// @InvalidScope({ "application" })
// public class GlobalUtils {
// /**
// * 判断对象释放为空
// *
// * @param o
// * @return
// */
// public static boolean isNUll(Object o) {
// return o == null;
// }
//
// /**
// * 全局是否debug状态
// *
// * @return
// */
// public static boolean isDebug() {
// return Strings.equalsIgnoreCase("localhost", Mvcs.getReq().getServerName())
// || Strings.equalsIgnoreCase("127.0.0.1", Mvcs.getReq().getServerName());
// }
//
// /**
// * 判断对象释放属于空白
// *
// * @param o
// * @return
// */
// public static boolean isBlank(Object o) {
// return o == null || Strings.isBlank(o.toString());
// }
//
// /**
// * 截断数字
// *
// * @param d
// * @param length
// * @return
// */
// public static String truncation(double d, int length) {
// return Numbers.format(d, length);
// }
//
// /**
// * 截断数字
// *
// * @param d
// * @param length
// * @return
// */
// public static String truncation(String d, int length) {
// return Numbers.format(d, length);
// }
//
// /**
// * 截断数字
// *
// * @param d
// * @param length
// * @return
// */
// public static String truncation(Object d, int length) {
// return Numbers.format(d.toString(), length);
// }
//
// /**
// * 判断字符串以特定串开头
// *
// * @param source
// * @param key
// * @return
// */
// public static boolean startWith(String source, Object key) {
// return source.startsWith(key.toString());
// }
// }
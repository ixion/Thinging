// package club.zhcs.tools;
//
// import java.util.List;
// import java.util.concurrent.ExecutionException;
//
// import org.apache.velocity.tools.config.DefaultKey;
// import org.apache.velocity.tools.config.InvalidScope;
// import org.nutz.ioc.IocException;
// import org.nutz.mvc.Mvcs;
//
// import club.zhcs.ext.menu.Menu;
// import club.zhcs.ext.menu.Menus;
// import club.zhcs.vo.Application;
//
/// **
// * @author 王贵源<kerbores>
// *
// * create at 2015年12月11日 下午1:54:43
// */
// @DefaultKey("M")
// @InvalidScope({ "application" })
// public class MenuUtils {
//
// /**
// * 当前登录用户菜单
// *
// * @return
// * @throws ExecutionException
// * @throws IocException
// */
// public static List<Menu> menus() throws IocException, ExecutionException {
// return
// Mvcs.getIoc().get(Menus.class).getCache().get(SessionUtils.loginUser());
// }
//
// public static String name() {
// return Application.NAME;
// }
//
// }

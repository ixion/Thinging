package club.zhcs.module;

import javax.servlet.http.HttpSession;

import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.nutz.lang.random.R;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Attr;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Param;

import com.kerbores.nutz.captcha.JPEGView;
import com.kerbores.nutz.module.base.AbstractBaseModule;
import com.kerbores.utils.entries.Result;
import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;

import club.zhcs.bean.auth.User;
import club.zhcs.service.auth.ShiroUserService;
import club.zhcs.service.auth.UserService;
import club.zhcs.vo.Application.SessionKeys;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 系统功能
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午3:36:42
 */
@At("system")
@Filters
public class SystemModule extends AbstractBaseModule {

	@Inject
	ShiroUserService shiroUserService;

	@Inject
	UserService userService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kerbores.nutz.module.base.AbstractBaseModule#_getNameSpace()
	 */
	@Override
	public String _getNameSpace() {
		return "system";
	}

	/**
	 * 用户登录
	 * 
	 * @param user
	 * @param password
	 * @param captcha
	 * @param session
	 * @return
	 *
	 * @author 王贵源
	 */
	@At
	public Result login(@Param("user") String user, @Param("password") String password, @Param("captcha") String captcha, HttpSession session) {
		if (Strings.equalsIgnoreCase(captcha, session.getAttribute(JPEGView.CAPTCHA).toString())) {
			Result result = shiroUserService.login(user, password);
			if (result.isSuccess()) {
				// 登录成功处理
				_putSession(SessionKeys.USER_KEY, result.getData().get("loginUser"));
			}
			return result;
		} else {
			return Result.fail("验证码输入错误");
		}
	}

	@At
	public Result sendSms(@Param("phone") String phone) throws ApiException {
		TaobaoClient client = new DefaultTaobaoClient("http://gw.api.taobao.com/router/rest", "23307391", "0d965b72f5914e2aac813fac49fc02aa");
		AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
		req.setSmsType("normal");
		req.setSmsFreeSignName("注册验证");
		int code = R.random(1000, 9999);
		req.setSmsParamString("{\"code\":\"" + code + "\",\"product\":\"ThinkBlog\",\"item\":\"ThinkBlog\"}");
		req.setRecNum(phone);
		req.setSmsTemplateCode("SMS_5041881");
		AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
		if (rsp.isSuccess()) {
			_putSession("code", code + "");
			_putSession("phone", phone);
			return Result.success().addData("code", code);
		}
		return Result.fail(rsp.getMsg());
	}

	/**
	 * 注册
	 * 
	 * @param userName
	 * @param password
	 * @param phone
	 * @param captcha
	 * @param code
	 * @return
	 *
	 * @author 王贵源
	 */
	@At
	public Result register(@Param("user") String userName, @Param("password") String password, @Param("captcha") String captcha, @Attr("code") String code,
			@Attr("phone") String phone) {
		if (Strings.equals(captcha, code)) {

			User user = new User();
			user.setName(userName);
			user.setPassword(Lang.md5(password));
			user.setPhone(phone);

			user = userService.addUser(user);
			return user == null ? Result.fail("注册失败") : Result.success().addData("user", user);
		}
		return Result.fail("验证码不正确");
	}

}

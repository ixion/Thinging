package club.zhcs.module;

import org.beetl.ext.nutz.BeetlViewMaker;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.mvc.View;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.By;
import org.nutz.mvc.annotation.ChainBy;
import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.IocBy;
import org.nutz.mvc.annotation.Modules;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import org.nutz.mvc.annotation.SetupBy;
import org.nutz.mvc.annotation.Views;
import org.nutz.mvc.filter.CheckSession;
import org.nutz.mvc.ioc.provider.ComboIocProvider;

import club.zhcs.ext.menu.Menus;
import club.zhcs.init.ZHCSSetup;
import club.zhcs.vo.Application.SessionKeys;

import com.kerbores.nutz.captcha.JPEGView;
import com.kerbores.nutz.module.base.AbstractBaseModule;
import com.kerbores.shiro.ShiroActionFilter;
import com.kerbores.utils.entries.Result;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 主模块
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午3:34:09
 */
@Modules(scanPackage = true)
@IocBy(type = ComboIocProvider.class, args = { "*anno", "com.kerbores", "club.zhcs", "*tx", "*js", "conf" })
@Views({ BeetlViewMaker.class })
@Fail("http:500")
@Ok("json")
@ChainBy(args = { "${app.root}/WEB-INF/chain/mychain.json" })
@Filters({ @By(type = ShiroActionFilter.class, args = "/"), @By(type = CheckSession.class, args = { SessionKeys.USER_KEY, "/" }) })
@SetupBy(ZHCSSetup.class)
public class MainModule extends AbstractBaseModule {

	@Override
	public String _getNameSpace() {
		return "main";
	}

	/**
	 * mvc 搭建测试
	 * 
	 * @return hello World!
	 *
	 * @author 王贵源
	 */
	@At
	@Filters
	public Result hello() {
		return Result.success().addData("msg", "Hello").addData("ip", _ip()).addData("browser", _ua().getBrowser()).addData("os", _ua().getOperatingSystem());
	}

	@Inject
	Dao dao;
	@Inject
	Menus menus;

	/**
	 * 验证码
	 * 
	 * @param length
	 *            验证码长度
	 * @return
	 *
	 * @author 王贵源
	 */
	@At
	@Filters
	public View captcha(@Param("length") int length) {
		return new JPEGView(null, length);
	}

	/**
	 * 登录页面
	 * 
	 * @return
	 *
	 * @author 王贵源
	 */
	@At
	@Filters
	@Ok("jsp:/login")
	public Result login() {
		return null;
	}

	/**
	 * 应用首页
	 * 
	 * @return
	 *
	 * @author 王贵源
	 */
	@At("/")
	@Filters
	@Ok("beetl:pages/blog/index.html")
	public Result index() {
		return Result.success();
	}

}

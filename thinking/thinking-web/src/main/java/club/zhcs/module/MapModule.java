package club.zhcs.module;

import org.nutz.lang.util.NutMap;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import com.kerbores.nutz.module.base.AbstractBaseModule;
import com.kerbores.utils.baidu.location.Baidus;
import com.kerbores.utils.baidu.location.Baidus.Location;
import com.kerbores.utils.entries.Result;

import club.zhcs.vo.Application;

/**
 * 
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 百度地图
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午3:36:02
 */
@At("map")
public class MapModule extends AbstractBaseModule {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kerbores.nutz.module.base.AbstractBaseModule#_getNameSpace()
	 */
	@Override
	public String _getNameSpace() {
		return "map";
	}

	/**
	 * 地图展示
	 * 
	 * @return
	 *
	 * @author 王贵源
	 */
	@At
	@Filters
	@Ok("beetl:pages/map/show.html")
	public Result show() {
		NutMap location = NutMap.NEW();
		System.err.println(_ip());
		Location l = Baidus.ip2Location(_ip());
		location.put("longitude", l.getLongitude());
		location.put("latitude", l.getLatitude());
		return Result.success().addData("location", location).addData("ak", Application.BAIDUMAPKEY);
	}

	/**
	 * 经纬度报告
	 * 
	 * @param longitude
	 * @param latitude
	 * @return
	 *
	 * @author 王贵源
	 */
	@At
	public Result report(@Param("longitude") double longitude, @Param("latitude") double latitude) {
		return Result.success();
	}

}

package club.zhcs.module.test;

import java.io.File;

import org.nutz.filepool.UU32FilePool;
import org.nutz.lang.Files;
import org.nutz.lang.random.R;
import org.nutz.mvc.View;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Filters;
import org.nutz.mvc.annotation.GET;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.POST;
import org.nutz.mvc.annotation.Param;

import com.kerbores.nutz.captcha.JPEGView;
import com.kerbores.nutz.module.base.AbstractBaseModule;
import com.kerbores.utils.entries.Result;

/**
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 文件下载测试
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年2月1日 下午10:02:06
 */
@At("/api/test")
@Filters
public class FileDownLoadModule extends AbstractBaseModule {

	UU32FilePool pool = new UU32FilePool("/tmp");

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kerbores.nutz.module.base.AbstractBaseModule#_getNameSpace()
	 */
	@Override
	public String _getNameSpace() {
		return null;
	}

	/**
	 * get 文件
	 * 
	 * @return
	 *
	 * @author 王贵源
	 */
	@At("/get/file")
	@GET
	@Ok("raw")
	public File getFile() {
		File file = pool.createFile("txt");
		Files.write(file, R.UU16());
		return file;
	}

	/**
	 * post 文件下载
	 *
	 * @param num
	 *            文件内容行数
	 * @return
	 *
	 * @author 王贵源
	 */
	@At("/post/file")
	@POST
	@Ok("raw")
	public File postFile(@Param("num") int num) {
		File file = pool.createFile("txt");
		for (int i = 0; i < num; i++) {
			Files.appendWrite(file, R.UU16() + "\r\n");
		}
		return file;
	}

	/**
	 * get json
	 * 
	 * @return
	 *
	 * @author 王贵源
	 */
	@At("/get/json")
	@GET
	public Result getJson() {
		return Result.success().addData("ip", _ip()).addData("ua", _ua());
	}

	/**
	 * postJson
	 * 
	 * @param num
	 * @return
	 *
	 * @author 王贵源
	 */
	@At("/post/json")
	@POST
	public Result postJson(@Param("num") int num) {
		return Result.success().addData("num", num);
	}

	/**
	 * 图片展示
	 * 
	 * @return
	 *
	 * @author 王贵源
	 */
	@At("/show/pic")
	public View showPic() {
		return new JPEGView(null);
	}

	/**
	 * post 显示验证码
	 * 
	 * @param length
	 * @return
	 *
	 * @author 王贵源
	 */
	@At("/show/pic")
	public View postShow(@Param("length") int length) {
		return new JPEGView(null, length);
	}

}

/**
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description 数据初始化包
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月26日 下午3:31:36
 */
package club.zhcs.init;
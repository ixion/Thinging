package club.zhcs.ws.u;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.Session;

/**
 * @author 王贵源
 *
 * @email kerbores@kerbores.com
 *
 * @description //TODO 类描述
 * 
 * @copyright 内部代码,禁止转发
 *
 *
 * @time 2016年1月29日 下午5:24:22
 */
public class SessionUtils {
	protected final static Map<String, Session> clients = new ConcurrentHashMap<>();

	public static void put(String relationId, int userCode, Session session) {
		clients.put(getKey(relationId, userCode), session);
	}

	public static Session get(String relationId, int userCode) {
		return clients.get(getKey(relationId, userCode));
	}

	public static void remove(String relationId, int userCode) {
		clients.remove(getKey(relationId, userCode));
	}

	/**
	 * 判断是否有连接
	 * 
	 * @param relationId
	 * @param userCode
	 * @return
	 */
	public static boolean hasConnection(String relationId, int userCode) {
		return clients.containsKey(getKey(relationId, userCode));
	}

	/**
	 * 组装唯一识别的key
	 * 
	 * @param relationId
	 * @param userCode
	 * @return
	 */
	public static String getKey(String relationId, int userCode) {
		return relationId + "_" + userCode;
	}
}

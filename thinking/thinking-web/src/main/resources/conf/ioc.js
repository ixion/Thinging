var ioc = {
    log: {
        type: "org.nutz.aop.interceptor.LoggingMethodInterceptor"
    },
    $aop: {
        type: "org.nutz.ioc.aop.config.impl.ComboAopConfigration",
        fields: {
            aopConfigrations: [
                {
                    type: "org.nutz.ioc.aop.config.impl.JsonAopConfigration",
                    fields: {
                        itemList: [
                            ["club\\.zhcs\\..+", ".+", "ioc:log"], //日志
                            //["com\\.tdb\\..+", "^(?!.*(_)).*$", "ioc:cache"],//缓存
                            ["club\\.zhcs\\.module\\..+", "^(?!.*(_)).*$", "ioc:nameSpace"], //模块命名空间
                            ["club\\.zhcs\\.module\\..+", "^(?!.*(_)).*$", "ioc:aopTimer"],//模块执行时间
                            ["club\\.zhcs\\.module\\..+", "^(?!.*(_)).*$", "ioc:txSERIALIZABLE"] //模块事务处理
                        ],
                    }
                }, {
                    type: "org.nutz.ioc.aop.config.impl.AnnotationAopConfigration"
                }]
        }
    }
};